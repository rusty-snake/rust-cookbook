GitLab is bullying its users more and more,
# Moved to <https://codeberg.org/rusty-snake/rust-cookbook> #

<br>


# Rust Cookbook

My rust cookbook. A list of good, useful crates (with thoughts and comments) +
boilerplate snippets.  
Have a look at the ["official" cookbook](https://rust-lang-nursery.github.io/rust-cookbook/) too.

### Beginner

 - [beginner_tools](https://crates.io/crates/beginner_tools) – Useful library designed for new Rustacens, provides good examples that are useful in simple projects

The python like `input()` function it provides is very good for beginners while
learning Rust.

```rust
use beginner_tools::input;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let name: String = input("Your name: ")?;
    println!("Hello {}!", name);
}
```

If you need a `input` function for other tasks then learning, it's often better
to hack your own one. Examples can be found at [snippets/src/input.rs](snippets/src/input.rs).

### Command line argument parsing

 - [clap](https://crates.io/crates/clap) – A simple to use, efficient, and full-featured Command Line Argument Parser
 - [structopt](https://crates.io/crates/structopt) – Parse command line argument by defining a struct.

### Concurrency, Parallelism and `async`/`.await`

 - [async-std](https://crates.io/crates/async-std) – Async version of the Rust standard library
 - [crossbeam](https://crates.io/crates/crossbeam) – Tools for concurrent programming
 - [futures](https://crates.io/crates/futures) – An implementation of futures and streams featuring zero allocations, composability, and iterator-like interfaces.
 - [rayon](https://crates.io/crates/rayon) – Simple work-stealing parallelism for Rust
 - [tokio](https://crates.io/crates/tokio) – An event-driven, non-blocking I/O platform for writing asynchronous I/O backed applications.

### D-Bus

 - [zbus](https://crates.io/crates/zbus) – API for D-Bus communication
 - [zvariant](https://crates.io/crates/zvariant) – D-Bus & GVariant encoding & decoding

### Errors

 - [anyhow](https://crates.io/crates/anyhow) – Flexible concrete Error type built on std::error::Error
 - [thiserror](https://crates.io/crates/thiserror) – derive(Error)

#### Which own should I use?

**TL;DR:** Use thiserror if the error should be handled by other Rust code
(e.g. libraries and library like code) and anyhow for anthing else
(where you just care about the representation that is shown to the user).

> #### Comparison to thiserror
>
> Use Anyhow if you don't care what error type your functions return, you just
> want it to be easy. This is common in application code. Use [thiserror] if you
> are a library that wants to design your own dedicated error type(s) so that on
> failures the caller gets exactly the information that you choose.
>
> [thiserror]: https://github.com/dtolnay/thiserror

<sup>src: https://github.com/dtolnay/anyhow#comparison-to-thiserror</sup>

> #### Comparison to anyhow
>
> Use thiserror if you care about designing your own dedicated error type(s) so
> that the caller receives exactly the information that you choose in the event of
> failure. This most often applies to library-like code. Use [Anyhow] if you don't
> care what error type your functions return, you just want it to be easy. This is
> common in application-like code.
>
> [Anyhow]: https://github.com/dtolnay/anyhow

<sup>src: https://github.com/dtolnay/thiserror#comparison-to-anyhow</sup>

### FFI

 - [bindgen](https://crates.io/crates/bindgen) – Automatically generates Rust FFI bindings to C and C++ libraries.
 - [cbindgen](https://crates.io/crates/cbindgen) – A tool for generating C bindings to Rust code.
 - [cc](https://crates.io/crates/cc) – A build-time dependency for Cargo build scripts to assist in invoking the native C compiler to compile native C code into a static archive to be linked into Rust code.
 - [pyo3](https://crates.io/crates/pyo3) – Bindings to Python interpreter

#### libc

 - [libc](https://crates.io/crates/libc) – Raw FFI bindings to platform libraries like libc.
 - [nix](https://crates.io/crates/nix) – Rust friendly bindings to *nix APIs

##### Pros of nix (negation is con of libc)

 - Rust friendly API
 - Safe where possible

##### Cons of nix (negation is pro of libc)

 - Not every libc function is implemented

### HTTP

 - [hyper](https://crates.io/crates/hyper) – A fast and correct HTTP library.
 - [reqwest](https://crates.io/crates/reqwest) – higher level HTTP client library

### Logging

 - [env_logger](https://crates.io/crates/env_logger) – A logging implementation for `log` which is configured via an environment variable.
 - [stderrlog](https://crates.io/crates/stderrlog) – Logger that logs to stderr based on verbosity specified
 - [log](https://crates.io/crates/log) – A lightweight logging facade for Rust

### Miscellaneous

 - [bitflags](https://crates.io/crates/bitflags) – A macro to generate structures which behave like bitflags.
 - [dirs](https://crates.io/crates/dirs) – A tiny low-level library that provides platform-specific standard locations of directories for config, cache and other data on Linux, Windows, macOS and Redox by leveraging the mechanisms defined by the XDG base/user directory specifications on Linux, the Known Folder API on Windows, and the Standard Directory guidelines on macOS.
 - [either](https://crates.io/crates/either) –  The enum `Either` with variants `Left` and `Right` is a general purpose sum type with two cases.
 - [itertools](https://crates.io/crates/itertools) – Extra iterator adaptors, iterator methods, free functions, and macros.
 - [lazy_static](https://crates.io/crates/lazy_static) – A macro for declaring lazily evaluated statics in Rust.
 - [rand](https://crates.io/crates/rand) – Random number generators and other randomness functionality.
 - [regex](https://crates.io/crates/regex) – An implementation of regular expressions for Rust. This implementation uses finite automata and guarantees linear time matching on all inputs.

#### thoughts, comments and remarks

##### either

either is good for quick-and-dirty code, however for anything else it is better
to write an own `enum` with more meaningful variants or to use a [Trait Object](https://doc.rust-lang.org/book/ch17-02-trait-objects.html).

##### lazy_static

For single-thread code you can also use [`thread_local!`](https://doc.rust-lang.org/std/macro.thread_local.html).

### Parsing

 - [addr](https://crates.io/crates/addr) – A library for parsing domain names
 - [semver](https://crates.io/crates/semver) – Semantic version parsing and comparison.
 - [serde](https://crates.io/crates/serde) – A generic serialization/deserialization framework
 - [serde_derive](https://crates.io/crates/serde_derive) – Macros 1.1 implementation of #[derive(Serialize, Deserialize)]
 - [serde_json](https://crates.io/crates/serde_json) – A JSON serialization file format
 - [toml](https://crates.io/crates/toml) – A native Rust encoder and decoder of TOML-formatted files and streams. Provides implementations of the standard Serialize/Deserialize traits for TOML data to facilitate deserializing and serializing Rust structures.
 - [url](https://crates.io/crates/url) – URL library for Rust, based on the WHATWG URL Standard

### Rust procedural macros

 - [syn](https://crates.io/crates/syn) – Parser for Rust source code
 - [quote](https://crates.io/crates/quote) – Quasi-quoting macro quote!(...)
 - [proc-macro2](https://crates.io/crates/proc-macro2) – A substitute implementation of the compiler's `proc_macro` API to decouple token-based libraries from the procedural macro use case.

### Terminal output

 - [color-backtrace](https://crates.io/crates/color-backtrace) – Colorful panic backtraces
 - [termcolor](https://crates.io/crates/termcolor) – A simple cross platform library for writing colored text to a terminal.
 - [textwrap](https://crates.io/crates/textwrap) – Powerful library for word wrapping, indenting, and dedenting strings
