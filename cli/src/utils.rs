//! Module for various programming helpers not directly related to the project.

use anyhow::anyhow;
use nix::libc;
use nix::unistd::{isatty, Uid, User};

/// Helper function used by structopt to parse `--color=<color-choice>`
pub fn parse_color_choice(color_choice: &str) -> anyhow::Result<termcolor::ColorChoice> {
    use termcolor::ColorChoice;

    match color_choice {
        "always" => Ok(ColorChoice::Always),
        "ansi" => Ok(ColorChoice::AlwaysAnsi),
        "auto" => {
            if isatty(libc::STDOUT_FILENO)? {
                Ok(ColorChoice::Auto)
            } else {
                Ok(ColorChoice::Never)
            }
        }
        "never" => Ok(ColorChoice::Never),
        invalid_choice => Err(anyhow!(
            "Invalid color choice '{}'. Valid choices are 'always', 'ansi', 'auto' and 'never'.",
            invalid_choice
        )),
    }
}

/// Get the name of the current user
pub fn get_user_name() -> anyhow::Result<String> {
    Ok(User::from_uid(Uid::current())?.expect("Unreachable").name)
}
