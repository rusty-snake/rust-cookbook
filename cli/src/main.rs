/*
 * rust-cookbook
 *
 * Written in 2021 by rusty-snake
 *
 * To the extent possible under law, the author(s) have dedicated all copyright and
 * related and neighboring rights to this software to the public domain worldwide.
 * This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along with
 * this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#![warn(rust_2018_idioms)]
#![deny(missing_debug_implementations)]

use std::cell::Cell;
use stderrlog::StdErrLog;
use structopt::StructOpt;
use termcolor::ColorChoice;

mod utils;

thread_local! {
    static COLOR_CHOICE: Cell<ColorChoice> = Cell::new(ColorChoice::Auto);
}

#[derive(StructOpt, Debug)]
#[structopt(about)]
struct Opt {
    /// Be quiet
    #[structopt(short = "q", long = "quiet")]
    quiet: bool,

    /// Be verbose (-v, -vv, -vvv, ...)
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    verbose: usize,

    /// Add timestamps to logging output
    #[structopt(
        long = "timestamp",
        overrides_with("timestamp"),
        default_value("off"),
        possible_values(&["sec", "ms", "us", "ns", "none", "off"]),
    )]
    timestamp: stderrlog::Timestamp,

    /// Specify when to use colored output
    #[structopt(
        long = "color",
        overrides_with("color-choice"),
        default_value("auto"),
        possible_values(&["always", "ansi", "auto", "never"]),
        parse(try_from_str = crate::utils::parse_color_choice),
    )]
    color_choice: ColorChoice,
}

fn main() {
    #[cfg(feature = "color-backtrace")]
    color_backtrace::install();

    let opt = Opt::from_args();

    COLOR_CHOICE.with(|color_choice| color_choice.set(opt.color_choice));

    StdErrLog::new()
        .verbosity(opt.verbose)
        .quiet(opt.quiet)
        .show_level(true)
        .timestamp(opt.timestamp)
        .color(opt.color_choice)
        .module(module_path!())
        .show_module_names(true)
        .init()
        .unwrap();

    log::debug!("Program successfull initialized");

    let user_name = match utils::get_user_name() {
        Ok(name) => name,
        Err(err) => {
            log::error!("Failed to get the username: {}", err);
            std::process::exit(1);
        }
    };
    println!("Hello, {}!", user_name);
}
