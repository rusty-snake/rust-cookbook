/*
 * rust-cookbook
 *
 * Written in 2021 by rusty-snake
 *
 * To the extent possible under law, the author(s) have dedicated all copyright and
 * related and neighboring rights to this software to the public domain worldwide.
 * This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along with
 * this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#![allow(unused_macros)]

mod functions {
    use std::fmt;
    use std::io::{Result as IoResult, Write};
    use termcolor::{ColorSpec, StandardStream, WriteColor};

    // cprint(Color::Magenta, "hello\n");
    fn cprint(color: termcolor::Color, msg: &str) -> IoResult<()> {
        let stdout =
            StandardStream::stdout(crate::COLOR_CHOICE.with(|color_choice| color_choice.get()));
        let mut stdout_lock = stdout.lock();
        stdout_lock.set_color(ColorSpec::new().set_fg(Some(color)))?;
        stdout_lock.write_all(msg.as_bytes())?;
        stdout_lock.reset()?;
        stdout_lock.flush()?;
        Ok(())
    }

    // cprintf(Color::Magenta, format_args!("{}\n", "hello"));
    fn cprintf(color: termcolor::Color, fmt: fmt::Arguments<'_>) -> IoResult<()> {
        let stdout =
            StandardStream::stdout(crate::COLOR_CHOICE.with(|color_choice| color_choice.get()));
        let mut stdout_lock = stdout.lock();
        stdout_lock.set_color(ColorSpec::new().set_fg(Some(color)))?;
        stdout_lock.write_fmt(fmt)?;
        stdout_lock.reset()?;
        stdout_lock.flush()?;
        Ok(())
    }
}

mod macros {
    // say!(Color::Magenta, bold=false, "{}\n", "hello");
    macro_rules! say {
        ($color:expr, bold = $bold:expr, $($arg:tt)*) => {{
            use ::std::io::Write;
            use ::termcolor::WriteColor;

            let stdout = ::termcolor::StandardStream::stdout(
                $crate::COLOR_CHOICE.with(|color_choice| color_choice.get()),
            );
            let mut stdout_lock = stdout.lock();
            stdout_lock.set_color(::termcolor::ColorSpec::new().set_fg(Some($color)).set_bold($bold)).unwrap();
            writeln!(stdout_lock, $($arg)*).unwrap();
            stdout_lock.reset().unwrap();
            stdout_lock.flush().unwrap();
        }};
    }
}

mod string_wrapper {
    use std::fmt;
    use std::io::Write;
    use termcolor::{Ansi, ColorSpec, WriteColor};

    // println!("{}", ColoredText::new("hello"));
    #[derive(Clone, Debug, Default, PartialEq, Eq, Hash)]
    pub struct ColoredText {
        inner: String,
    }
    impl ColoredText {
        fn _new(color_spec: &ColorSpec, text: &str) -> Self {
            let mut buffer = Ansi::new(Vec::<u8>::with_capacity(text.len() + 16));
            buffer.set_color(color_spec).unwrap();
            buffer.write_all(text.as_bytes()).unwrap();
            buffer.reset().unwrap();
            buffer.get_mut().shrink_to_fit();

            Self {
                // SAFETY: ANSI escape sequences are valid unicode and the rest is a str.
                inner: unsafe { String::from_utf8_unchecked(buffer.into_inner()) },
            }
        }

        /// Create a new `ColoredText`
        pub fn new<T: AsRef<str>>(color: termcolor::Color, text: T) -> Self {
            Self::_new(ColorSpec::new().set_fg(Some(color)), text.as_ref())
        }

        /// Create a new bold `ColoredText`
        pub fn bold<T: AsRef<str>>(color: termcolor::Color, text: T) -> Self {
            Self::_new(
                ColorSpec::new().set_bold(true).set_fg(Some(color)),
                text.as_ref(),
            )
        }

        /// Get a references to the underlying String
        pub fn get_ref(&self) -> &String {
            &self.inner
        }

        /// Get a mutable references to the underlying String
        ///
        /// **Warning:** Be care full when editing this String, at the begin and end are
        /// ANSI-escape sequences. If you edit them, you might get ugly output.
        ///
        pub fn get_mut(&mut self) -> &mut String {
            &mut self.inner
        }

        #[inline]
        pub fn as_bytes(&self) -> &[u8] {
            self.inner.as_bytes()
        }

        #[inline]
        pub fn as_str(&self) -> &str {
            self.inner.as_str()
        }

        #[inline]
        pub fn into_bytes(self) -> Vec<u8> {
            self.inner.into_bytes()
        }

        #[inline]
        pub fn into_string(self) -> String {
            self.inner
        }
    }
    impl fmt::Display for ColoredText {
        #[inline]
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            fmt::Display::fmt(&self.inner, f)
        }
    }
    impl AsRef<[u8]> for ColoredText {
        #[inline]
        fn as_ref(&self) -> &[u8] {
            self.inner.as_bytes()
        }
    }
    impl AsRef<str> for ColoredText {
        #[inline]
        fn as_ref(&self) -> &str {
            self.inner.as_str()
        }
    }
}

mod write_colored_trait {
    use std::io::Result as IoResult;
    use termcolor::WriteColor;

    // io::stdout().write_colored(Color::Magenta, b"hello")?;
    pub trait WriteColored {
        fn write_colored(&mut self, spec: termcolor::ColorSpec, buf: &[u8]) -> IoResult<()>;
    }
    impl<W: WriteColor> WriteColored for W {
        fn write_colored(&mut self, spec: termcolor::ColorSpec, buf: &[u8]) -> IoResult<()> {
            self.set_color(&spec)?;
            self.write_all(buf)?;
            self.reset()?;
            Ok(())
        }
    }
}

mod string_with_color_and_colorized_trait {
    use std::env;
    use std::fmt;
    use std::io::prelude::*;
    use termcolor::{Buffer, Color, ColorChoice, ColorSpec, WriteColor};

    // println!("{}", "hello".magenta());
    #[derive(Clone, Debug, Default, PartialEq, Eq)]
    pub struct ColoredString {
        pub color_spec: termcolor::ColorSpec,
        pub string: String,
    }
    impl ColoredString {
        fn color_choice() -> bool {
            match crate::COLOR_CHOICE.with(|color_choice| color_choice.get()) {
                ColorChoice::Always | ColorChoice::AlwaysAnsi => true,
                ColorChoice::Auto => {
                    // env-var-is-set: "TERM" && $TERM != "dump" && env-var-is-not-set: "NO_COLOR"
                    env::var_os("TERM").filter(|term| term == "dump").is_some()
                        && env::var_os("NO_COLOR").is_none()
                }
                ColorChoice::Never => false,
            }
        }
    }
    impl fmt::Display for ColoredString {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            let mut buf = if Self::color_choice() {
                Buffer::ansi()
            } else {
                Buffer::no_color()
            };
            buf.set_color(&self.color_spec).map_err(|_| fmt::Error)?;
            buf.write_all(self.string.as_bytes())
                .map_err(|_| fmt::Error)?;
            buf.reset().map_err(|_| fmt::Error)?;
            f.write_str(unsafe { std::str::from_utf8_unchecked(buf.as_slice()) })
        }
    }

    pub trait Colorize {
        fn colored(self, color: termcolor::Color) -> ColoredString
        where
            Self: ToString + Sized,
        {
            let mut color_spec = ColorSpec::new();
            color_spec.set_fg(Some(color));

            ColoredString {
                color_spec,
                string: self.to_string(),
            }
        }

        fn red(self) -> ColoredString
        where
            Self: ToString + Sized,
        {
            Self::colored(self, Color::Red)
        }

        fn green(self) -> ColoredString
        where
            Self: ToString + Sized,
        {
            Self::colored(self, Color::Green)
        }

        fn blue(self) -> ColoredString
        where
            Self: ToString + Sized,
        {
            Self::colored(self, Color::Blue)
        }
    }
    impl<T: ToString + Sized> Colorize for T {}
}

mod str_with_color_and_paint_trait {
    use std::env;
    use std::fmt;
    use std::io::prelude::*;
    use termcolor::{Buffer, ColorChoice, ColorSpec, WriteColor};

    // println!("{}", Color::Magenta.paint("hello"));
    #[derive(Clone, Debug, Default, PartialEq, Eq)]
    pub struct ColoredStr<'a> {
        pub color_spec: termcolor::ColorSpec,
        pub string: &'a str,
    }
    impl ColoredStr<'_> {
        fn color_choice() -> bool {
            match crate::COLOR_CHOICE.with(|color_choice| color_choice.get()) {
                ColorChoice::Always | ColorChoice::AlwaysAnsi => true,
                ColorChoice::Auto => {
                    // if env-var-is-set: "TERM" && $TERM != "dump" && env-var-is-not-set NO_COLOR
                    env::var_os("TERM").filter(|term| term == "dump").is_some()
                        && env::var_os("NO_COLOR").is_none()
                }
                ColorChoice::Never => false,
            }
        }
    }
    impl fmt::Display for ColoredStr<'_> {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            let mut buf = if ColoredStr::color_choice() {
                Buffer::ansi()
            } else {
                Buffer::no_color()
            };
            buf.set_color(&self.color_spec).map_err(|_| fmt::Error)?;
            buf.write_all(self.string.as_bytes())
                .map_err(|_| fmt::Error)?;
            buf.reset().map_err(|_| fmt::Error)?;
            f.write_str(unsafe { std::str::from_utf8_unchecked(buf.as_slice()) })
        }
    }

    pub trait Paint {
        fn paint(self, s: &str) -> ColoredStr<'_>;
    }
    impl Paint for termcolor::Color {
        fn paint(self, s: &str) -> ColoredStr<'_> {
            let mut color_spec = ColorSpec::new();
            color_spec.set_fg(Some(self));
            ColoredStr {
                color_spec,
                string: s,
            }
        }
    }
}
