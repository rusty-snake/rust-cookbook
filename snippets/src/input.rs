/*
 * rust-cookbook
 *
 * Written in 2021 by rusty-snake
 *
 * To the extent possible under law, the author(s) have dedicated all copyright and
 * related and neighboring rights to this software to the public domain worldwide.
 * This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along with
 * this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

mod not_trimming {
    use std::io;
    use std::io::prelude::*;

    fn input(prompt: &str) -> io::Result<String> {
        let mut stdout = io::stdout();
        stdout.write_all(prompt.as_bytes())?;
        stdout.flush()?;

        let mut string = String::new();
        io::stdin().read_line(&mut string)?;
        string.truncate(string.len() - 1);
        string.shrink_to_fit();
        Ok(string)
    }
}

mod trimming {
    use std::io;
    use std::io::prelude::*;

    fn input(prompt: &str) -> io::Result<String> {
        let mut stdout = io::stdout();
        stdout.write_all(prompt.as_bytes())?;
        stdout.flush()?;

        let mut strbuf = String::new();
        io::stdin().read_line(&mut strbuf)?;
        if let Some(idx) = strbuf.find(|c: char| !c.is_whitespace()) {
            strbuf.replace_range(..idx, "");
        }
        if let Some(idx) = strbuf.rfind(|c: char| !c.is_whitespace()) {
            strbuf.replace_range(idx + 1.., "");
        }
        strbuf.shrink_to_fit();
        Ok(strbuf)
    }
}
