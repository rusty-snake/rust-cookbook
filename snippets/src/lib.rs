/*
 * rust-cookbook
 *
 * Written in 2021 by rusty-snake
 *
 * To the extent possible under law, the author(s) have dedicated all copyright and
 * related and neighboring rights to this software to the public domain worldwide.
 * This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along with
 * this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#![allow(dead_code)]

mod input;
#[path = "termcolor-wrappers.rs"]
mod termcolor_wrappers;

use std::cell::Cell;
use termcolor::ColorChoice;

thread_local! {
    static COLOR_CHOICE: Cell<ColorChoice> = Cell::new(ColorChoice::Auto);
}
